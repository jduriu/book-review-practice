from django.contrib import admin
from .models import Book, BookFavorite, Genre

# Model Admin Objects
class BookAdmin(admin.ModelAdmin):
    pass

class BookFavoriteAdmin(admin.ModelAdmin):
    pass

class GenreAdmin(admin.ModelAdmin):
    pass

admin.site.register(Book, BookAdmin)
admin.site.register(BookFavorite, BookFavoriteAdmin)
admin.site.register(Genre, GenreAdmin)
