from django.db import models
from django.conf import settings


AUTH_USER = settings.AUTH_USER_MODEL


class Book(models.Model):
    author = models.ForeignKey(
        AUTH_USER,
        # related_name="books"
        on_delete=models.CASCADE,
        null=True,
    )
    genre = models.ManyToManyField("Genre", blank=True)
    title = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.title

class Genre(models.Model):
    name = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.name

class BookFavorite(models.Model):
    user = models.ForeignKey(
        AUTH_USER,
        on_delete=models.CASCADE,
        null=True,
    )
    book = models.ForeignKey(
        "Book",
        # related_name = "book",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return f"{self.book} by {self.user}"
