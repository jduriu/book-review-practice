from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.core.paginator import Paginator
from .models import Book

from .forms import BookForm, BookEditForm

# Create your views here.
def books_list(request):
    list_of_books = Book.objects.all()
    paginator = Paginator(list_of_books, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        "books": list_of_books,
        "page_obj": page_obj,
    }

    return render(request, 'books/list.html', context)

def create_book(request):
    if request.method == "POST":
        form = BookForm(request.POST)

        if form.is_valid():
            form.save()
            return render(request, "books/list.html")
    else:
        form = BookForm()
    context = {
        "form": form,
    }
    return render(request, 'books/new.html', context)

def book_detail(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
        "book":book,
    }
    return render(request, "books/detail.html", context)

def book_delete(request, pk):
    if request.method == "POST":
        book = Book.objects.get(pk=pk)
        book.delete()
        success_url = reverse_lazy("books_list")
        return success_url

def book_edit(request, pk):
    if request.method == "POST":
        form = BookEditForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("books_list")
    else:
        form = BookEditForm()
    context = {
        "form": form
        }
    return render(request, "books/edit.html", context)
