from django.urls import path

from books.views import (
    books_list,
    create_book,
    book_detail,
    book_edit,
    book_delete,
)

urlpatterns = [
    path("", books_list, name="books_list"),
    path("new/", create_book, name="new_book"),
    path("<int:pk>/", book_detail, name="book_detail"),
    path("<int:pk>/delete", book_delete, name="book_delete"),
    path("<int:pk>/edit", book_edit, name="book_edit"),
]
