from django import forms

try:
    from books.models import Book

    class BookForm(forms.ModelForm):
        class Meta:
            model = Book
            fields = [
                "author",
                "genre",
                "title",
            ]

    class BookEditForm(forms.ModelForm):
        class Meta:
            model = Book
            fields = "__all__"

except Exception:
    pass
